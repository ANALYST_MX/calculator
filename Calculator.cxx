#include "Calculator.hxx"

double Calculator::calculate()
{
    double  dOperand2    = m_stk.pop().toDouble();
    QString strOperation = m_stk.pop();
    double  dOperand1    = m_stk.pop().toDouble();
    double  dResult      = 0;

    if (strOperation == "+") {
        dResult = dOperand1 + dOperand2;
    }
    if (strOperation == "-") {
        dResult = dOperand1 - dOperand2;
    }
    if (strOperation == "/") {
        dResult = dOperand1 / dOperand2;
    }
    if (strOperation == "*") {
        dResult = dOperand1 * dOperand2;
    }
    return dResult;
}

void Calculator::clear()
{
  if (!m_stk.empty())
    {
      m_stk.clear();
    }
}

void Calculator::push(QString &value)
{
  m_stk.push(value);
}

void Calculator::push(double value)
{
  m_stk.push(QString().setNum(value));
}

int Calculator::count()
{
  return m_stk.count();
}
