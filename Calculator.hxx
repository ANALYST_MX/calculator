#pragma once

#include <QString>
#include <QStack>

class Calculator
{
public:
  double calculate();
  void clear();
  void push(QString &);
  void push(double);
  int count();

private:
  QStack<QString> m_stk;
};
