#-------------------------------------------------
#
# Project created by QtCreator 2015-10-20T12:16:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Calculator
TEMPLATE = app


SOURCES += main.cxx\
        MainWindow.cxx \
    Calculator.cxx

HEADERS  += MainWindow.hxx \
    main.hxx \
    Calculator.hxx

CONFIG += c++11
