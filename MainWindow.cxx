#include "MainWindow.hxx"

MainWindow::MainWindow(QWidget *parent)
  : QWidget(parent), calc(new Calculator)
{
  m_plcd = new QLCDNumber(12);
  m_plcd->setSegmentStyle(QLCDNumber::Flat);
  m_plcd->setMinimumSize(150, 50);

  QChar aButtons[4][4] = {{'7', '8', '9', '/'},
                          {'4', '5', '6', '*'},
                          {'1', '2', '3', '-'},
                          {'0', '.', '=', '+'}
                         };


  QGridLayout* ptopLayout = new QGridLayout;
  ptopLayout->addWidget(m_plcd, 0, 0, 1, 4);
  ptopLayout->addWidget(createButton("CE"), 1, 3);

  for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) {
          ptopLayout->addWidget(createButton(aButtons[i][j]), i + 2, j);
        }
    }
  setLayout(ptopLayout);
}

MainWindow::~MainWindow()
{
  delete calc;
}

QPushButton* MainWindow::createButton(const QString& str)
{
  QPushButton *pcmd = new QPushButton(str);
  pcmd->setMinimumSize(40, 40);
  connect(pcmd, SIGNAL(clicked()), SLOT(slotButtonClicked()));

  return pcmd;
}

void MainWindow::slotButtonClicked()
{
  QString str = ((QPushButton*)sender())->text();
  if (str == "CE")
    {
      calc->clear();
      m_strDisplay = "";
      m_plcd->display("0");
    }
  else if (str.contains(QRegExp("[0-9]")))
    {
      m_strDisplay += str;
      m_plcd->display(m_strDisplay.toDouble());
    }
  else if (str == ".")
    {
      if (m_strDisplay.indexOf('.') == -1)
        {
          m_strDisplay += str;
          m_plcd->display(m_strDisplay);
        }
    }
  else {
      if (calc->count() >= 2)
        {
          calc->push(m_plcd->value());
          m_plcd->display(calc->calculate());
          calc->clear();
          calc->push(m_plcd->value());
          if (str != "=") {
              calc->push(str);
            }
        }
      else
        {
          calc->push(m_plcd->value());
          calc->push(str);

          m_strDisplay = "";
          m_plcd->display("0");
        }
    }
}

