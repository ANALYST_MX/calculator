#pragma once

#include <QLCDNumber>
#include <QtWidgets>
#include "Calculator.hxx"

class MainWindow : public QWidget {
    Q_OBJECT
public:
    MainWindow(QWidget * = nullptr);
    ~MainWindow();

public slots:
    void slotButtonClicked();

private:
    QLCDNumber *m_plcd;
    QString m_strDisplay;
    QPushButton *createButton(const QString &);
    Calculator *calc;
};
